@extends('layouts.master')

@section('Judul Tab')
Master Data Vaksin
@endsection

@section('Isi Halaman')
<!-- Row -->
<div class="container-fluid" id="container-wrapper">    
<div class="row">
            <!-- Datatables -->
            <div class="col-lg-12">
              <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Data Vaksin</h6>
                </div>
                <div class="table-responsive p-3">
                    <table class="table">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">No Tiket</th>
                            <th scope="col">Nama Peserta</th>
                            <th scope="col">Tanggal Lahir</th>
                            <th scope="col">Merek Vaksin</th>
                            <th scope="col">Nama Faskes</th>
                            <th scope="col">Lokasi</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse ($data_vaksins as $key=>$value)
                                <tr>
                                    <td>{{$key + 1}}</th>
                                    <td>{{$value->no_tiket}}</td>
                                    <td>{{$value->dataPeserta->nama_peserta}}</td>
                                    <td>{{$value->dataPeserta->tanggal_lahir}}</td>
                                    <td>{{$value->tanggal_vaksin}}</td>
                                    <td>{{empty($value->dataJenisVaksin)?'':$value->dataJenisVaksin->nama}}</td>
                                    <td>{{empty($value->dataFaskes)?'':$value->dataFaskes->nama_faskes}}</td>
                                    <td>
                                    <a href="/data/show/{{$value->id}}" class="btn btn-info">Show</a>
                                        {{-- <a href="/data/show/{{$value->id}}" class="btn btn-primary">Edit</a>
                                        <form action="/data/delete/{{$value->id}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                                        </form> --}}
                                    </td>
                                </tr>
                            @empty
                                <tr colspan="3">
                                    <td>No data</td>
                                </tr>  
                            @endforelse              
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
</div>
</div>

        
            
@endsection