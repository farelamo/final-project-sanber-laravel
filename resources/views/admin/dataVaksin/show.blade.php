@extends('layouts.master')


@section('Judul Tab')
Data Vaksinasi 
@endsection


@section('Isi Halaman')

<div class="container-fluid" id="container-wrapper">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"></h3>
        </div>
        <div class="card-body">

            <h6 class="m-0 font-weight-bold text-primary">Kartu Vaksinasi</h6>

            <br>

              <div class="form-group">
                <label for="title">No Tiket</label>
                <input type="text" class="form-control" name="peserta_id" value="{{$data->no_tiket}}" id="peserta_id" readonly>
                    <label for="title">Nama Peserta</label>
                    <input type="text" class="form-control" name="peserta_id" value="{{$data->dataPeserta->nama_peserta}}" id="peserta_id" readonly>
                    <label for="body">Nama Vaksin</label>
                    <input type="text" class="form-control" name="jenis_vaksin_id"  value="{{$data->dataJenisVaksin->nama}}"  id="jenis_vaksin_id" readonly>
                    <label for="body">Tanggal Vaksin</label>
                    <input type="text" class="form-control" name="faskes_id"  value="{{$data->tanggal_vaksin}}"  id="faskes_id" readonly>
                    <label for="body">Dosis Vaksin</label>
                    <input type="text" class="form-control" name="faskes_id"  value="{{$data->dosis}}"  id="faskes_id" readonly>
                    <label for="body">Lokasi Vaksin</label>
                    <input type="text" class="form-control" name="faskes_id"  value="{{$data->dataFaskes->nama_faskes}}"  id="faskes_id" readonly>
                    <br>
                    <br>
                    <a href="/peserta/cetak_pdf/{{$data->id}}" class="btn btn-primary pull-right btn-sm"> Download Kartu Vaksin</a>
                </div>

   
@endsection