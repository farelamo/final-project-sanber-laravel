@extends('layouts.master')

@section('Judul Tab')
Master - Faskes - Add
@endsection

{{-- @section('Judul Halaman')
Tambah Master Data Fasilitas Kesehatan
@endsection --}}



@section('Isi Halaman')

<div class="container-fluid" id="container-wrapper">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>
        </div>
        <div class="card-body">
  
            <br>
              
            <form id="faskes-create" action="/faskes/store" method="POST">
                @csrf

                <div class="form-group">
                    <label for="title">Nama Fasilitas Kesehatan</label>
                    <input type="text" class="form-control" name="nama_faskes" id="nama_faskes" placeholder="Masukkan Nama Fasilitas Kesehatan">
                    @error('nama_faskes')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            
            
                <div class="form-group">
                    <label for="title">Alamat Fasilitas Kesehatan</label>
                    <input type="text" class="form-control" name="alamat_faskes" id="alamat_faskes" placeholder="Masukkan Alamat Fasilitas Kesehatan">
                    @error('nama_faskes')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>


                <div class="form-group">
                    <label for="body">Nama Penanggung Jawab</label>
                    <input type="text"  class="form-control" name="penanggung_jawab" id="penanggung_jawab" placeholder="Masukkan Nama Penanggung Jawab"> </textarea>
                    @error('penanggung_jawab')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            
                <button type="submit" class="btn btn-primary">Tambah</button>

        </div>
      <br>
    </div>




    <script> 


        swal({
            title: "Deleted!",
            text: "Your row has been deleted.",
            button: "Close", // Text on button
            icon: "success", //built in icons: success, warning, error, info
            timer: 3000, //timeOut for auto-close
            buttons: {
                confirm: {
                text: "OK",
                value: true,
                visible: true,
                className: "",
                closeModal: true
                },
                cancel: {
                text: "Cancel",
                value: false,
                visible: true,
                className: "",
                closeModal: true,
                }
                }
            });


    </script>


@endsection