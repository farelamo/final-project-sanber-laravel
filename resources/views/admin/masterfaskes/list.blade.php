@extends('layouts.master')


@section('Judul Tab')
Master - Faskes
@endsection

{{-- @section('Judul Halaman')
Master Data Fasilitas Kesehatan
@endsection --}}



@section('Isi Halaman')


<div class="container-fluid" id="container-wrapper">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>
        </div>
        <div class="card-body">
  


              
            <div class="row">
                <div class="col-lg-12">
                    <a href="/faskes/create" align="center" class="btn btn-primary pull-right btn-sm">Tambah Fasilitas Kesehatan</a>
                    <a href="/faskes/create" align="center" class="btn btn-primary pull-right btn-sm">Export to Pdf</a>
                </div>
            </div>
            <br>



        <!-- Row -->
        <div class="row">
            <!-- Datatables -->
            <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">DataTables</h6>
                </div>
                <div class="table-responsive p-3">
                <table class="table align-items-center table-flush" id="dataTable">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Updated At</th>
                        <th scope="col">Nama Faskes</th>
                        <th scope="col">Alamat Faskes</th>
                        <th scope="col">Penanggung Jawab</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                
                    <tbody>
                    
                        @forelse ($list_faskes as $key=>$value)
                        <tr>
                            <td>{{$key + 1}}</th>
                            <td>{{$value->created_at}}</td>
                            <td>{{$value->updated_at}}</td>
                            <td>{{$value->nama_faskes}}</td>
                            <td>{{$value->alamat_faskes}}</td>
                            <td>{{$value->penanggung_jawab}}</td>
                            <td>
                                <a href="/faskes/show/{{$value->id}}" class="btn btn-primary">Edit</a>
                               
                                <form action="/faskes/soft-delete/{{$value->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">

                                </form>

               

         
                            </td>
                        </tr>
                    @empty
                        <tr colspan="5">
                            <td>No data</td>
                        </tr>  
                    @endforelse



                    </tbody>
                </table>
                </div>
            </div>
            </div>





        </div>
      <br>
    </div>




    <script>
        $(document).ready(function () {
          $('#dataTable').DataTable(); // ID From dataTable 
          $('#dataTableHover').DataTable(); // ID From dataTable with Hover
        });




        // display a modal (small modal)
        $(document).on('click', '#smallButton', function(event) {
                event.preventDefault();
                let href = $(this).attr('data-attr');
                $.ajax({
                    url: href
                    , beforeSend: function() {
                        $('#loader').show();
                    },
                    // return the result
                    success: function(result) {
                        $('#smallModal').modal("show");
                        $('#smallBody').html(result).show();
                    }
                    , complete: function() {
                        $('#loader').hide();
                    }
                    , error: function(jqXHR, testStatus, error) {
                        console.log(error);
                        alert("Page " + href + " cannot open. Error:" + error);
                        $('#loader').hide();
                    }
                    , timeout: 8000
                })
            });


      </script>


@endsection