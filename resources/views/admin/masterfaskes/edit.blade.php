@extends('layouts.master')


@section('Judul Tab')
Master - Faskes - Edit
@endsection

@section('Judul Halaman')
Edit Master Data Fasilitas Kesehatan
@endsection



@section('Isi Halaman')


<div class="container-fluid" id="container-wrapper">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>
        </div>
        <div class="card-body">
        <br>



        <form action="/faskes/update/{{$data_faskes->id}}" method="POST">

                    @csrf
                    @method('put')


                    <div class="form-group">
                        <label for="title">Nama Fasilitas Kesehatan</label>
                        <input type="text" class="form-control" name="nama_faskes" id="nama_faskes" value={{$data_faskes->nama_faskes}}>
                        {{-- @error('nama_faskes')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror --}}
                    </div>
                    <div class="form-group">
                        <label for="body">Alamat faskes</label>
                        <input type="text" class="form-control" name="alamat_faskes" id="alamat_faskes" value={{$data_faskes->alamat_faskes}}>
                        {{-- @error('alamat_faskes')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror --}}
                    </div>

                    <div class="form-group">
                        <label for="body">Nama Penanggung Jawab</label>
                        <input type="text" class="form-control" name="penanggung_jawab" id="penanggung_jawab" value={{$data_faskes->penanggung_jawab}}>
                
                    </div>



                    <button type="submit" class="btn btn-primary">Update</button>
                </form>

    <br>
</div>


@endsection