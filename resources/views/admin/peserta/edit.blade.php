@extends('layouts.master')




@section('Judul Tab')
Peserta - Edit
@endsection

@section('Judul Halaman')
Edit Peserta Vaksinasi
@endsection



@section('Isi Halaman')




<div class="container-fluid" id="container-wrapper">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>
        </div>
        <div class="card-body">
  
            <br>
            
 
            <form id="peserta-create" action="/peserta/update/{{$data_peserta->id}}" method="POST">
                @csrf
                @method('put')

                <div class="form-group">
                    <label for="title">NIK</label>
                    <input type="text" class="form-control" name="nik" id="nik"  value={{$data_peserta->nik}}>
                    @error('nik')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            

                <div class="form-group">
                    <label for="title">Nama Peserta</label>
                    <input type="text" class="form-control" name="nama_peserta" id="nama_peserta" value={{$data_peserta->nama_peserta}}>
                    @error('nama_peserta')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

        


                  <div class="form-group">
                    <label for="title">Tanggal Lahir</label>
                    <input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir" value={{$data_peserta->tanggal_lahir}}>
                    @error('tanggal_lahir')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>



                <div class="form-group">
                    <label for="title">Pilih Jenis Kelamin</label>
                    <select  id ="cmb_jenis_kelamin" name="cmb_jenis_kelamin" class="form-control">
                        <option value="L">Laki - Laki</option>
                        <option value="P">Perempuan</option>
                      </select>
                </div>



                <div class="form-group">
                    <label for="title">No HP</label>
                    <input type="text" class="form-control" name="no_hp" id="no_hp" value={{$data_peserta->no_hp}}>
                    @error('no_hp')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>



                <div class="form-group">
                    <label for="title">Alamat</label>
                    <textarea class="form-control" name="alamat_peserta" id="alamat_peserta" value={{$data_peserta->alamat_peserta}}> </textarea>
                    @error('alamat_peserta')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="title">RT</label>
                    <input type="text" class="form-control" name="rt" id="rt" value={{$data_peserta->rt}} >
                    @error('rt')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>


                <div class="form-group">
                    <label for="title">RW</label>
                    <input type="text" class="form-control" name="rw" id="rw" value={{$data_peserta->rw}}>
                    @error('rw')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>


                <div class="form-group">
                    <label for="title">Pilih Provinsi</label>
                    <select  id ="cmb_propinsi" name="cmb_propinsi" class="form-control">
    
                        @forelse ($data_propinsi as $key=>$value)

                        <option value={{$value->kd_propinsi}}>{{$value->nama_propinsi}}</option>
                    
                        @empty
                        <option value="0">0</option>
                        @endforelse
    
                      </select>


                </div>




                
                <div class="form-group">
                    <label for="title">Pilih Kota Kabupaten</label>
                    <select  id ="cmb_kabkota" name="cmb_kabkota" class="form-control">
                    </select>
                </div>


                <div class="form-group">
                    <label for="title">Pilih Kecamatan</label>
                    <select  id ="cmb_kecamatan" name="cmb_kecamatan" class="form-control">
                    </select>
                </div>


                <div class="form-group">
                    <label for="title">Pilih Kelurahan</label>
                    <select  id ="cmb_kelurahan" name="cmb_kelurahan" class="form-control">
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Update</button>

        </div>
      <br>
    </div>






   
    <script>




        // $(document).ready(function(){
        //     $('#myDropDown').change(function(){
        //         //Selected value
        //         var inputValue = $(this).val();
        //         alert("value in js "+inputValue);

        //         //Ajax for calling php function
        //         $.post('/getpropinsi', { dropdownValue: inputValue }, function(data){
        //             alert('ajax completed. Response:  '+data);
        //             //do after submission operation in DOM
        //         });
        //     });
        // });




        $(document).ready(function(){

            $("#cmb_propinsi").change(function(){
                var propinsi_id = $(this).val();
                $.ajax({
                    url: '/getkabkota',
                    type: 'get',
                    data: {propinsi_id:propinsi_id},
                    dataType: 'json',
                    success:function(response){

                        console.log(response);

                        var len = response.data_kabkota.length;

                        if (len>0) {

                                    $("#cmb_kabkota").empty();
                                    for( var i = 0; i<len; i++){
                                        var id = response.data_kabkota[i]['kd_kotakabupaten'];
                                        var name = response.data_kabkota[i]['nama_kotakabupaten'];
                                    $("#cmb_kabkota").append("<option value='"+id+"'>"+name+"</option>");

                                }
                   
                         }
                    }
                });
            });

        });





         $(document).ready(function(){

            $("#cmb_kabkota").change(function(){
                var kabkota_id = $(this).val();
                $.ajax({
                    url: '/getkecamatan',
                    type: 'get',
                    data: {kabkota_id:kabkota_id},
                    dataType: 'json',
                    success:function(response){

                        console.log(response);

                        var len = response.data_kecamatan.length;

                        if (len>0) {

                                    $("#cmb_kecamatan").empty();
                                    for( var i = 0; i<len; i++){
                                        var id = response.data_kecamatan[i]['kd_kecamatan'];
                                        var name = response.data_kecamatan[i]['nama_kecamatan'];
                                    $("#cmb_kecamatan").append("<option value='"+id+"'>"+name+"</option>");

                                }
                
                        }
                    }
                });
            });

            });





            $(document).ready(function(){

                $("#cmb_kecamatan").change(function(){
                    var kecamatan_id = $(this).val();
                    $.ajax({
                        url: '/getkelurahan',
                        type: 'get',
                        data: {kecamatan_id:kecamatan_id},
                        dataType: 'json',
                        success:function(response){

                            console.log(response);

                            var len = response.data_kelurahan.length;

                            if (len>0) {

                                        $("#cmb_kelurahan").empty();
                                        for( var i = 0; i<len; i++){
                                            var id = response.data_kelurahan[i]['kd_kelurahan'];
                                            var name = response.data_kelurahan[i]['nama_kelurahan'];
                                        $("#cmb_kelurahan").append("<option value='"+id+"'>"+name+"</option>");

                                    }
                            }

                        }
                    });
                });

            });

        



    </script>




@endsection