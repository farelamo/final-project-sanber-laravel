@extends('layouts.master')


@section('Judul Tab')
Peserta - Vaksin
@endsection

{{-- @section('Judul Halaman')
Peserta Vaksinasi
@endsection --}}



@section('Isi Halaman')

<div class="container-fluid" id="container-wrapper">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"></h3>
        </div>
        <div class="card-body">

            <div class="row">
                <div class="col-lg-12">
                    <a href="/peserta/create" class="btn btn-primary pull-right btn-sm">Tambah Peserta</a>
                    <a href="/peserta/cetak_pdf" class="btn btn-primary pull-right btn-sm"> Download Ke Pdf</a>
                </div>
  
            </div>

            <br>

        <!-- Row -->
        <div class="row">
            <!-- Datatables -->
            <div class="col-lg-12">
            <div class="card mb-4">
      
                <div class="table-responsive p-3">
                <table class="table align-items-center table-flush" id="dataTable">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">NIK</th>
                        <th scope="col">Nama Peserta</th>
                        <th scope="col">Tanggal Lahir</th>
                        <th scope="col">No. Hp</th>
                        <th scope="col">Alamat Peserta</th>
                        <th scope="col">RT</th>
                        <th scope="col">RW</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                
                    <tbody>
                    
                        @forelse ($list_peserta as $key=>$value)
                        <tr>
                            <td>{{$key + 1}}</th>
                            <td>{{$value->nik}}</td>
                            <td>{{$value->nama_peserta}}</td>
                            <td>{{$value->tanggal_lahir}}</td>
                            <td>{{$value->no_hp}}</td>
                            <td>{{$value->alamat_peserta}}</td>
                            <td>{{$value->rt}}</td>
                            <td>{{$value->rw}}</td>
                            <td>
                                <a href="/peserta/show/{{$value->id}}" class="btn btn-primary">Edit</a>
                                <a href="/peserta/register-show/{{$value->id}}" class="btn btn-primary">REG</a>
                                <form action="/peserta/soft-delete/{{$value->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr colspan="9">
                            <td>No data</td>
                        </tr>  
                    @endforelse
                    </tbody>
                </table>
                </div>
            </div>
            </div>



                </tbody>
            </table>
        </div>
        <br>
    </div>


    
    <script>
        $(document).ready(function () {
          $('#dataTable').DataTable(); // ID From dataTable 
          $('#dataTableHover').DataTable(); // ID From dataTable with Hover
        });




        // display a modal (small modal)
        $(document).on('click', '#smallButton', function(event) {
                event.preventDefault();
                let href = $(this).attr('data-attr');
                $.ajax({
                    url: href
                    , beforeSend: function() {
                        $('#loader').show();
                    },
                    // return the result
                    success: function(result) {
                        $('#smallModal').modal("show");
                        $('#smallBody').html(result).show();
                    }
                    , complete: function() {
                        $('#loader').hide();
                    }
                    , error: function(jqXHR, testStatus, error) {
                        console.log(error);
                        alert("Page " + href + " cannot open. Error:" + error);
                        $('#loader').hide();
                    }
                    , timeout: 8000
                })
            });


      </script>



@endsection