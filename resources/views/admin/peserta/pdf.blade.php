<html>
<head>
	<title>Laporan Peserta Vaksin</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>
	<center>
		<h5>Daftar Peserta Vaksin</h4>
	
	</center>
 
       <!-- Row -->
       <div class="row">
        <!-- Datatables -->
        <div class="col-lg-12">
        <div class="card mb-4">
  
            <div class="table-responsive p-3">
            <table class="table align-items-center table-flush" id="dataTable">
                <thead class="thead-light">
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">NIK</th>
                    <th scope="col">Nama Peserta</th>
                    <th scope="col">Tanggal Lahir</th>
                    <th scope="col">No. Hp</th>
                    <th scope="col">Alamat Peserta</th>
                    <th scope="col">RT</th>
                    <th scope="col">RW</th>
                </tr>
                </thead>
            
                <tbody>
                
                    @forelse ($list_peserta as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nik}}</td>
                        <td>{{$value->nama_peserta}}</td>
                        <td>{{$value->tanggal_lahir}}</td>
                        <td>{{$value->no_hp}}</td>
                        <td>{{$value->alamat_peserta}}</td>
                        <td>{{$value->rt}}</td>
                        <td>{{$value->rw}}</td>
                       
                    </tr>
                @empty
                    <tr colspan="9">
                        <td>No data</td>
                    </tr>  
                @endforelse


                </tbody>
            </table>
            </div>
        </div>
        </div>
 











        <script>
            $(document).ready(function () {
              $('#dataTable').DataTable(); // ID From dataTable 
              $('#dataTableHover').DataTable(); // ID From dataTable with Hover
            });
    
    
    
    
          </script>
    
</body>
</html>