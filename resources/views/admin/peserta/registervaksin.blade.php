@extends('layouts.master')




@section('Judul Tab')
Registrasi Peserta Vaksin - Add
@endsection

@section('Judul Halaman')
Registrasi  Peserta Vaksinasi
@endsection



@section('Isi Halaman')




<div class="container-fluid" id="container-wrapper">
    <div class="card">
        <div class="card-header">
          <h3 class="card-title"></h3>
        </div>
        <div class="card-body">
  
            <br>
            
 
            <form id="peserta-create" action="/peserta/register/vaksin" method="POST">
                @csrf

    
                <div class="form-group">
                    <input type="hidden" class="form-control" name="peserta_id" id="peserta_id"  value={{$data_peserta->id}} readonly="readonly">
                    <label for="title">NIK</label>
                    <input type="text" class="form-control" name="nik" id="nik"  value={{$data_peserta->nik}} readonly="readonly">
                </div>
            

                <div class="form-group">
                    <label for="title">Nama Peserta</label>
                    <input type="text" class="form-control" name="nama_peserta" id="nama_peserta" value={{$data_peserta->nama_peserta}} disabled>
                 </div>

        


                  <div class="form-group">
                    <label for="title">Tanggal Lahir</label>
                    <input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir" value={{$data_peserta->tanggal_lahir}} disabled>
                 </div>



                <div class="form-group">
                    <label for="title">Jenis Kelamin </label>
                    <input type="date" class="form-control" name="tanggal_lahir" id="tanggal_lahir" value={{$data_peserta->tanggal_lahir}} disabled>
                  
                </div>

    



                <div class="form-group">
                    <label for="title">No HP</label>
                    <input type="text" class="form-control" name="no_hp" id="no_hp" value={{$data_peserta->no_hp}} disabled>
    
                </div>



                <div class="form-group">
                    <label for="title">Alamat</label>
                    <textarea class="form-control" name="alamat_peserta" id="alamat_peserta" value={{$data_peserta->alamat_peserta}} disabled> </textarea>
                </div>

                <div class="form-group">
                    <label for="title">RT</label>
                    <input type="text" class="form-control" name="rt" id="rt" value={{$data_peserta->rt}} disabled>

                    <label for="title">RW</label>
                    <input type="text" class="form-control" name="rw" id="rw" value={{$data_peserta->rw}} disabled>
     
                </div>


     

                <div class="form-group">
                    <label for="title">No Tiket</label>
                    <input type="text" class="form-control" name="no_tiket" id="no_tiket" placeholder="Masukkan Nomor Tiket Vaksin">
                    @error('no_tiket')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>



                <div class="form-group">
                    <label for="title">Pilih Jenis Vaksin</label>
                    <select  id ="cmb_jenis_vaksin" name="cmb_jenis_vaksin" class="form-control">
    
                        @forelse ($data_vaksin as $key=>$value)

                        <option value={{$value->id}}>{{$value->merk}}</option>
                    
                        @empty
                        <option value=0>Tidak Ada</option>
                        @endforelse
    
                      </select>

                </div> 


                <div class="form-group">
                    <label for="title">Tanggal Vaksin</label>
                    <input type="date" class="form-control" name="tanggal_vaksin" id="tanggal_vaksin" >
                 </div>




                <div class="form-group">
                    <label for="title">Pilih Dosis</label>
                    <select  id ="cmb_dosis" name="cmb_dosis" class="form-control">
                        <option value=1>Dosis Ke 1</option>
                        <option value=2>Dosis Ke 2</option>
                        <option value=3>Dosis Ke 3 (Booster)</option>
                      </select>
                </div>


                
                <div class="form-group">
                    <label for="title">Pilih Fasilitas Kesehatan</label>
                    <select  id ="cmb_faskes" name="cmb_faskes" class="form-control">
    
                    </select>

                </div>



             

                <button type="submit" class="btn btn-primary">Register</button>

        </div>
      <br>
    </div>






   
    <script>



            $(document).ready(function(){

                $("#cmb_jenis_vaksin").click(function(){
                    var vaksin_id = $(this).val();
                    $.ajax({
                        url: '/getFaskes',
                        type: 'get',
                        data: {vaksin_id:vaksin_id},
                        dataType: 'json',
                        success:function(response){

                            console.log(response);

                            var len = response.data_faskes.length;

                            if (len>0) {

                                        $("#cmb_faskes").empty();
                                        for( var i = 0; i<len; i++){
                                            var id = response.data_faskes[i]['id'];
                                            var name = response.data_faskes[i]['nama_faskes'];
                                        $("#cmb_faskes").append("<option value='"+id+"'>"+name+"</option>");

                                    }
                            }

                        }
                    });
                });

            });

        



    </script>




@endsection