@extends('layouts.master')

@section('Judul Tab')
Master Vaksin Ok
@endsection

@section('Isi Halaman')
<div>
    <h2>Tambah Data</h2>
        <form action="/vaksin/store" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan nama">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="merk">Merk</label>
                <input type="text" class="form-control" name="merk" id="merk" placeholder="Masukkan merk">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="asal">Asal Vaksin</label>
                <input type="text" class="form-control" name="asal_vaksin" id="asal_vaksin" placeholder="Masukkan asal">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection