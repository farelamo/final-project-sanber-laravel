@extends('layouts.master')


@section('Judul Tab')
Master - Faskes - Edit
@endsection


@section('Isi Halaman')
<div>
        <h2>Edit Vaksin {{$vaksin->id}}</h2>
        <form action="/vaksin/update/{{$vaksin->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$vaksin->nama}}" id="nama" placeholder="Masukkan Nama">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Merek</label>
                <input type="text" class="form-control" name="merk"  value="{{$vaksin->merk}}"  id="body" placeholder="Masukkan Merk">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Asal Vaksin</label>
                <input type="text" class="form-control" name="asal_vaksin"  value="{{$vaksin->asal_vaksin}}"  id="body" placeholder="Masukkan Asal">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection