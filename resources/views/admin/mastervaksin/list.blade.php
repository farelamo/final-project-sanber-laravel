@extends('layouts.master')

@section('Judul Tab')
Master - Vaksin
@endsection

@section('Isi Halaman')
<!-- Row -->
<div class="container-fluid" id="container-wrapper">    
<div class="row">
            <!-- Datatables -->
            <div class="col-lg-12">
              <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Jenis Vaksin</h6>
                </div>
                <div class="table-responsive p-3">
                <a href="/vaksin/create" class="btn btn-primary">Tambah</a>
                    <table class="table">
                        <thead class="thead-light">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Merk</th>
                            <th scope="col">Asal Vaksin</th>
                            <th scope="col">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse ($jenis_vaksins as $key=>$value)
                                <tr>
                                    <td>{{$key + 1}}</th>
                                    <td>{{$value->nama}}</td>
                                    <td>{{$value->merk}}</td>
                                    <td>{{$value->asal_vaksin}}</td>
                                    <td>
                                        <a href="/vaksin/show/{{$value->id}}" class="btn btn-primary">Edit</a>
                                        <form action="/vaksin/delete/{{$value->id}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr colspan="3">
                                    <td>No data</td>
                                </tr>  
                            @endforelse              
                        </tbody>
                    </table>
                </div>
              </div>
            </div>
</div>
</div>

        
            
@endsection