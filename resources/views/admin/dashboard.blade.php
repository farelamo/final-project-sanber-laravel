@extends('layouts.master')


@section('Judul Tab')
Dashboard
@endsection

@section('Judul Halaman')
Ini adalah Halaman Dashboard Layout
@endsection



@section('Isi Halaman')

<h1 class="text-center"> WELCOME TO ADMIN DASHBOARD, {{ Auth::user()->name }} !!</h1>
<br>
<div class="container-fluid" id="container-wrapper">
  <div class="row mb-3">
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card h-100">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-uppercase mb-1">Peserta Vaksin</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $peserta }}</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-file-medical-alt fa-2x text-success"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card h-100">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
       
              <div class="text-xs font-weight-bold text-uppercase mb-1">Dosis 1</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $dosis1 }}</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-stethoscope fa-2x text-success"></i>
            </div>

            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-uppercase mb-1">Dosis 2</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $dosis2 }}</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-stethoscope fa-2x text-success"></i>
            </div>

            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-uppercase mb-1">Dosis 3</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $dosis3 }}</div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card h-100">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-uppercase mb-1">Master Faskes</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $faskes }}</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-notes-medical fa-2x text-danger"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="col-xl-3 col-md-6 mb-4">
      <div class="card h-100">
        <div class="card-body">
          <div class="row no-gutters align-items-center">
            <div class="col mr-2">
              <div class="text-xs font-weight-bold text-uppercase mb-1">Master Vaksin</div>
              <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $jenis_vaksin}}</div>
            </div>
            <div class="col-auto">
              <i class="fas fa-notes-medical fa-2x text-danger"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>

@endsection