<!DOCTYPE html>
<html lang="en">
  @include('user.layouts.head')
  <!-- body -->
  
  <body class="main-layout inner_page">
      <!-- loader  -->
      <div class="loader_bg">
          <div class="loader"><img src="{{ asset('user/images/loading.gif')}}" alt="#" /></div>
      </div>
      <!-- end loader -->
      <!-- top -->
      <!-- header -->
      <header class="header-area">
          <div class="left">
              <a href="Javascript:void(0)"><i class="fa fa-search" aria-hidden="true"></i></a>
          </div>
          <div class="right">
              <a href="/login"><i class="fa fa-user" aria-hidden="true"></i></a>
          </div>
          <div class="container">
              <div class="row d_flex">
                  <div class="col-sm-3 logo_sm">
                      <div class="logo">
                          <a href="/"></a>
                      </div>
                  </div>
                  <div class="col-lg-10 offset-lg-1 col-md-12 col-sm-9">
                      <div class="navbar-area">
                          <nav class="site-navbar">
                              <ul>
                                  <li><a href="/">Home</a></li>
                                  <li><a class="active" href="/about">About</a></li>
                                  <li><a href="/action">take action</a></li>
                                  <li><a href="/" class="logo_midle">covido</a></li>
                                  <li><a href="/news">news</a></li>
                                  <li><a href="/doctores">doctores</a></li>
                                  <li><a href="/contact">Contact </a></li>
                              </ul>
                              <button class="nav-toggler">
                                  <span></span>
                              </button>
                          </nav>
                      </div>
                  </div>
              </div>
          </div>
      </header>
      <!-- end header -->
      <!-- about -->
      <div class="about">
         <div class="container_width">
            <div class="row d_flex">
                   <div class="col-md-7">
                  <div class="titlepage text_align_left">
                     <h2>About Corona Virus </h2>
                     <p>English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for
                     </p>
                     <a class="read_more" href="about.html">About More</a>
                  </div>
               </div>
               <div class="col-md-5">
                  <div class="about_img text_align_center">
                     <figure><img src="{{ asset('user/images/about.png')}}" alt="#"/></figure>
                  </div>
               </div>
              
            </div>
         </div>
      </div>
      <!-- end about -->
      <!--  footer -->
      @include('user.layouts.footer')
   </body>
</html>