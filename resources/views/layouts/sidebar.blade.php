<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
      <div class="sidebar-brand-icon">
        <img src="/admin/img/logo/logo2.png">
      </div>
      <div class="sidebar-brand-text mx-3">RuangAdmin</div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item">
      <a class="nav-link" href="/home">
        <i class="fas fa-user-md"></i>
        <span>Dashboard</span></a>
    </li>
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
      Features
    </div>



    <li class="nav-item">
        <a class="nav-link" href="/peserta">
          <i class="fas fa-file-medical-alt"></i>
          <span>Peserta Vaksin </span></a>
    </li>


    <li class="nav-item">
        <a class="nav-link" href="/data">
          <i class="fas fa-stethoscope"></i>
          <span>Data Vaksinasi </span></a>
    </li>


    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseForm" aria-expanded="true"
        aria-controls="collapseForm">
        <i class="fas fa-notes-medical"></i>
        <span>Master Data</span>
      </a>
      <div id="collapseForm" class="collapse" aria-labelledby="headingForm" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <a class="collapse-item" href="/faskes">Master Faskes</a>
          <a class="collapse-item" href="/vaksin">Master Vaksin</a>
        </div>
      </div>
    </li>

    {{-- <li class="nav-item">
        <a class="nav-link" href="/user">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>User Login </span></a>
    </li> --}}


    <hr class="sidebar-divider">
    
    {{-- <a href="/user" class="btn btn-primary">
       Portal Vaksin
    </a> --}}

    <div class="p-3">
      <a href="/" class="btn btn-primary btn-lg btn-block btn-icon-split">
        <i class=""></i> Portal Vaksin
      </a>
    </div>
  </ul>