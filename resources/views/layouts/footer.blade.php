<footer class="sticky-footer bg-white">
    <div class="container my-auto">
      <div class="copyright text-center my-auto">
        <span>copyright &copy; <script> document.write(new Date().getFullYear()); </script> - developed by
          <b><a href="https://gitlab.com/farelamo/final-project-sanber-laravel" target="_blank">Kelompok 6 Sanbercode</a></b>
        </span>
      </div>
    </div>
  </footer>