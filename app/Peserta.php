<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DataVaksin;
use App\Faskes;


class Peserta extends Model
{
    //
    protected $table = "peserta";

    protected $primaryKey = 'id';

    public function dataVaksin(){
        return $this->hasMany('App\DataVaksin');
    }




}
