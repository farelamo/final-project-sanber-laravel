<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Peserta;
use App\JenisVaksin;
Use App\Faskes;

class DataVaksin extends Model
{
    //

    protected $table="data_vaksin";



  public function dataV()
  {
   return $this->belongsTo('App\peserta','id','peserta_id');
  }  

  public function dataFaskes(){
    return $this->belongsTo(Faskes::class,'faskes_id','id');
    }

  public function dataJenisVaksin()
  {
   return $this->belongsTo('App\JenisVaksin','jenis_vaksin_id','id');
  }

  public function dataPeserta()
  {
   return $this->belongsTo('App\Peserta','peserta_id','id');
  }



}


