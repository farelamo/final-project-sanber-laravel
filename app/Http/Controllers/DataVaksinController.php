<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\DataVaksin;
use App\Peserta; 
use App\Faskes;
use App\JenisVaksin;
use Barryvdh\DomPDF\Facade as PDF;

class DataVaksinController extends Controller
{
    public function index()
    {
        // $data_vaksins = DB::table('data_vaksin')->get();
        // return view('admin.dataVaksin.list', compact('data_vaksins'));
        
        // $data_vaksins = DataVaksin::all();

            // $data_vaksins = $data->dataVaksinPeserta();



            // $data_vaksins = Peserta::with(['dataVaksin','dataVaksin.dataFaskes','dataVaksin.dataJenisVaksin'])->get()->toArray();
            $data_vaksins = DataVaksin::all();
 
            // $trx_vaksin->dataVaksin;
             
            // dd($data_vaksins);die;
      
             return view('admin.dataVaksin.list',compact('data_vaksins'));
    







    }

        public function show($id)
        {
            $data = DataVaksin::find($id);
            return view('admin.dataVaksin.show', compact('data'));
        }



        public function cetak_pdf($id)
        {
         
            $data = DataVaksin::find($id);
     
     
            $pdf = PDF::loadview('admin.dataVaksin.pdf',['data'=>$data]);
            return $pdf->download('kartuvaksin.pdf');
        }
     
     
}
