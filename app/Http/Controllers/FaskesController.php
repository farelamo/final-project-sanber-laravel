<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Faskes;

class FaskesController extends Controller
{
    //

  

    public function index (){

        // $list_faskes = DB::table('faskes')->get();
        // return view('admin.masterfaskes.list', compact('list_faskes'));


        $list_faskes=Faskes::where('status_enabled','Y')
                            ->orderBy('nama_faskes')
                            ->take(10)
                            ->get();

        return view('admin.masterfaskes.list', compact('list_faskes'));

    }

    




    
    public function create (){

        return view ('admin.masterfaskes.create');
           }

    
    public function store (Request $request){

        // dd($request->all()); die;
        
        $request->validate([
        'nama_faskes'=>'required',
        'alamat_faskes'=>'required',
        'penanggung_jawab'=>'required',

        ]);

        
        $faskes= new Faskes;

        $faskes->nama_faskes=$request['nama_faskes'];
        $faskes->alamat_faskes=$request['alamat_faskes'];
        $faskes->penanggung_jawab=$request['penanggung_jawab'];
        $faskes->status_enabled='Y';
        $faskes->save();
        return  redirect ('/faskes');
   

    }
  


    public function show_edit_by_id($id)
    {
        $data_faskes=Faskes::find($id);  
              
        return view('admin.masterfaskes.edit', compact('data_faskes'));
    }


    public function update($id, Request $request)
    {
        $request->validate([
            'nama_faskes'=>'required',
            'alamat_faskes'=>'required',
            'penanggung_jawab'=>'required',
    
            ]);

        $update_faskes = Faskes::where('id',$id)->update ([

            "nama_faskes"=>$request['nama_faskes'],
            "alamat_faskes"=>$request['alamat_faskes'],
            "penanggung_jawab"=>$request['penanggung_jawab']
                
            ]);

        return redirect('/faskes')->with('Success','Update Data Berhasil');

    }



    public function softdelete($id, Request $request)
    {

        $update_faskes = Faskes::where('id',$id)->update ([

            "status_enabled"=>'N',
                
            ]);

        return redirect('/faskes')->with('Success','Data Berhasil Di Hapus');
    }


    
    public function popupDelete($id)
    {

        $delete = Faskes::find($id);

        return view('masterfaskes.popup-delete', compact('delete'));
    }







}
