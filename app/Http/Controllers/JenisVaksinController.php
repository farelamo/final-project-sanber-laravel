<?php

namespace App\Http\Controllers;
use DB;
use App\JenisVaksin;

use Illuminate\Http\Request;

class JenisVaksinController extends Controller
{
    public function index()
    {
        $jenis_vaksins = DB::table('jenis_vaksin')->get();
        return view('admin.mastervaksin.list', compact('jenis_vaksins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.mastervaksin.create');
    }

    public function store(Request $request)
    {
        // $request->validate([
        //   'nama' => 'required',
        //  'merk' => 'required',
        //  'asal_vaksin' => 'required',
        // ]);

        $vaksin= new JenisVaksin;

        $vaksin->nama=$request['nama'];
        $vaksin->merk=$request['merk'];
        $vaksin->asal_vaksin=$request['asal_vaksin'];
        $vaksin->status_enabled='Y';
        $vaksin->save();
        return  redirect ('/vaksin');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JenisVaksin  $jenisVaksin
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JenisVaksin  $jenisVaksin
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vaksin = JenisVaksin::find($id);
        return view('admin.mastervaksin.edit', compact('vaksin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JenisVaksin  $jenisVaksin
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        // $request->validate([
        //  'nama' => 'required',
        //  'merk' => 'required',
        //  'asal_vaksin' => 'required',
        // ]);

        $vaksin = JenisVaksin::find($id);
        $vaksin->nama = $request->nama;
        $vaksin->merk = $request->merk;
        $vaksin->asal_vaksin = $request->asal_vaksin;
        $vaksin->update();
        return redirect('/vaksin')->with('Success','Update Data Berhasil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JenisVaksin  $jenisVaksin
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {

        $update_vaksin = JenisVaksin::where('id',$id)->update ([

            "status_enabled"=>'N',
                
            ]);

        return redirect('/vaksin')->with('Success','Data Berhasil Di Hapus');
    }
}
