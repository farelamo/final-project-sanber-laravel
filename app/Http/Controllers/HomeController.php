<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peserta;
use App\JenisVaksin;
use App\Faskes;
use App\DataVaksin;

use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $peserta = Peserta::count();
        // $jenisvaksin = JenisVaksin::count();
        // $faskes = Faskes::count();
        // $peserta = DB::table('peserta')->count()-> where('status_enabled', 'Y');
        $peserta=Peserta::where('status_enabled','Y')->count();
        $faskes = Faskes::where('status_enabled','Y')->count();
        $dosis1 = DataVaksin::where('dosis',1)->count();
        $dosis2 = DataVaksin::where('dosis',2)->count();
        $dosis3 = DataVaksin::where('dosis',3)->count();
        $jenis_vaksin = DB::table('jenis_vaksin')->count();
        return view('admin.dashboard', compact('peserta', 'faskes', 'jenis_vaksin','dosis1','dosis2','dosis3'));
    }
}
