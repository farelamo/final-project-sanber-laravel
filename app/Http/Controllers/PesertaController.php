<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Propinsi;
use App\KabKota;
use App\Kecamatan;
use App\Kelurahan;
use App\Peserta;
use App\JenisVaksin;
use App\Faskes;
use App\DataVaksin;
use Barryvdh\DomPDF\Facade as PDF;

class PesertaController extends Controller
{

    public function index (){

        $list_peserta=Peserta::where('status_enabled','Y')
        ->orderBy('nama_peserta')
        ->get();

        return view('admin.peserta.list', compact('list_peserta'));
 

    }

    public function create (){

    
        $data_propinsi = propinsi::all();
        return view('admin.peserta.create', compact('data_propinsi'));
     

    }

    public function store (Request $request){

        // dd($request->all()); die;
        
        // $request->validate([
        //     'nama_peserta'=>'required',
        //     'jenis_kelamin'=>'required',
        //     'no_hp'=>'required',
        //     'alamat_peserta'=>'required',
        //     'rt'=>'required',
        //     'rw'=>'required'
        //     ]);



            $peserta= new Peserta;
            $peserta->nik=$request['nik'];
            $peserta->nama_peserta=$request['nama_peserta'];
            $peserta->alamat_peserta=$request['alamat_peserta'];
            $peserta->tanggal_lahir=$request['tanggal_lahir'];
            $peserta->jenis_kelamin=$request['cmb_jenis_kelamin'];
            $peserta->no_hp=$request['no_hp'];
            $peserta->alamat_peserta=$request['alamat_peserta'];
            $peserta->rt=$request['rt'];
            $peserta->rw=$request['rw'];
            $peserta->kdprovinsi=$request['cmb_propinsi'];
            $peserta->kdkabkota=$request['cmb_kabkota'];
            $peserta->kdkecamatan=$request['cmb_kecamatan'];
            $peserta->kdkelurahan=$request['cmb_kelurahan'];
            $peserta->status_enabled='Y';
            $peserta->save();

            return  redirect ('/peserta');
    }

    public function show_edit_by_id($id)
    {
        
        $data_peserta=Peserta::find($id);  

        $data_propinsi = propinsi::all();
        // return view('admin.peserta.create', compact('data_propinsi'));
              
        return view('admin.peserta.edit', compact('data_peserta'), compact('data_propinsi'));
    }


    public function update($id, Request $request)
    {

        $update_peserta = Peserta::where('id',$id)->update ([

            "nik"=>$request['nik'],
            "nama_peserta"=>$request['nama_peserta'],
            "alamat_peserta"=>$request['alamat_peserta'],
            "tanggal_lahir"=>$request['tanggal_lahir'],
            "jenis_kelamin"=>$request['cmb_jenis_kelamin'],
            "no_hp"=>$request['no_hp'],
            "alamat_peserta"=>$request['alamat_peserta'],
            "rt"=>$request['rt'],
            "rw"=>$request['rw'],
            "kdprovinsi"=>$request['cmb_propinsi']

                
            ]);

        return redirect('/peserta')->with('Success','Update Data Berhasil');

    }

    public function show_register_by_id($id, Request $request)
    {
        
                
        $data_peserta=Peserta::find($id);  
        
        $data_vaksin=JenisVaksin::where('status_enabled','Y')
        ->orderBy('merk')
        ->get();

    

        return view('admin.peserta.registervaksin', compact('data_peserta'),compact('data_vaksin'));
     
    }












    public function register(Request $request)
    {




        try {
            $data_vaksin= new DataVaksin;
            $data_vaksin->no_tiket_vaksin=$request['no_tiket'];
            $data_vaksin->peserta_id=$request['peserta_id'];
            $data_vaksin->jenis_vaksin_id=$request['cmb_jenis_vaksin'];
            $data_vaksin->tanggal_vaksin=$request['tanggal_vaksin'];
            $data_vaksin->faskes_id=$request['cmb_faskes'];
            $data_vaksin->dosis=$request['cmb_dosis'];
            $data_vaksin->save();
        } catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }
        return  redirect ('/peserta');
                       
       

   

    }




    
    public function softdelete($id,Request $request)
    {
      
        $update_peserta = Peserta::where('id',$id)->update ([

            "status_enabled"=>'N',
                
            ]);

        return redirect('/peserta')->with('Success','Data Berhasil Di Hapus');

    }





    public function getPropinsi (){

    
        $data_propinsi = propinsi::all();
        return compact('data_propinsi');
     

    }



    public function getKabKotaByProp_Id (){

        $id=$_GET['propinsi_id'];
    
         $data_kabkota=kabkota::where('kd_propinsi',$id)
                    ->orderBy('nama_kotakabupaten')
                    ->get();
                
        return compact('data_kabkota');

    }



    public function getKecamatanByKabKota_Id (){

        $id=$_GET['kabkota_id'];
    
         $data_kecamatan=kecamatan::where('kd_kotakabupaten',$id)
                    ->orderBy('nama_kecamatan')
                    ->get();
                
        return compact('data_kecamatan');
    }


    public function getKelurahanByKecamatan_Id (){

         $id=$_GET['kecamatan_id'];
    
         $data_kelurahan=kelurahan::where('kd_kecamatan',$id)
                    ->orderBy('nama_kelurahan')
                    ->get();
                
        return compact('data_kelurahan');
    }



    public function getFaskes (){

        $data_faskes=Faskes::where('status_enabled','Y')
        ->orderBy('nama_faskes')
        ->get();

        return compact('data_faskes');

   }



   public function cetak_pdf()
   {
    
       $list_peserta=Peserta::where('status_enabled','Y')
       ->orderBy('nama_peserta')
       ->get();


       $pdf = PDF::loadview('admin.peserta.pdf',['list_peserta'=>$list_peserta]);
       return $pdf->download('laporan-peserta.pdf');
   }





}
