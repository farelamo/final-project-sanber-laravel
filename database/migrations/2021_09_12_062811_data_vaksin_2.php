<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataVaksin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_vaksin_2', function (Blueprint $table){
            $table->bigIncrements('id');
            $table->UnsignedBigInteger('peserta_vaksin_id');
            $table->UnsignedBigInteger('merk_vaksin_id');
            $table->date('tanggal_vaksin', -1);
            $table->string('dosis_vaksin', 255);
            $table->UnsignedBigInteger('faskes_id');
            $table->string('no_tiket_vaksin', 255);
            $table->foreign('peserta_vaksin_id')->references('id')->on('peserta_vaksin');
            $table->foreign('merk_vaksin_id')->references('id')->on('merk_vaksin');
            $table->foreign('faskes_id')->references('id')->on('faskes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_vaksin_2');
    }
}
