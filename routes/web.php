<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => ['auth']], function () {
    Route::get('/login', function () {
        return view('auth.login');
    });

    Route::get('/home', 'HomeController@index');

    
// Untuk Mengelola Modul Master Faskes
// Route::resource('faskes', 'FaskesController');

Route::get('/faskes', 'FaskesController@index');

Route::get('/faskes/create', 'FaskesController@create');
Route::post('/faskes/store', 'FaskesController@store');

Route::get('/faskes/show/{faskes_id}', 'FaskesController@show_edit_by_id');
Route::put('/faskes/update/{faskes_id}', 'FaskesController@update');

Route::delete('/faskes/soft-delete/{faskes_id}', 'FaskesController@softdelete');


Route::get('/faskes/popup-delete/{faskes_id}', 'FaskesController@popupDelete');


//END MASTER FASKES

// Untuk Mengelola Modul Peserta Vaksin//
// Route::resource('peserta', 'PesertaController');
Route::get('/peserta', 'PesertaController@index');

Route::get('/peserta/create', 'PesertaController@create');
Route::post('/peserta/store', 'PesertaController@store');

Route::get('/peserta/show/{peserta_id}', 'PesertaController@show_edit_by_id');
Route::put('/peserta/update/{peserta_id}', 'PesertaController@update');

Route::get('/peserta/register-show/{peserta_id}', 'PesertaController@show_register_by_id');
Route::get('/getFaskes', 'PesertaController@getFaskes');
Route::post('/peserta/register/vaksin', 'PesertaController@register');

Route::delete('/peserta/soft-delete/{peserta_id}', 'PesertaController@softdelete');
Route::get('/peserta/cetak_pdf', 'PesertaController@cetak_pdf');
//END PESERTA VAKSIN




//Get data wilayah AJAX
Route::get('/getpropinsi', 'PesertaController@getPropinsi');
Route::get('/getkabkota', 'PesertaController@getKabKotaByProp_Id');
Route::get('/getkecamatan', 'PesertaController@getKecamatanByKabKota_Id');
Route::get('/getkelurahan', 'PesertaController@getKelurahanByKecamatan_Id');




// Untuk Mengelola Modul Master Vaksin//
Route::get('/vaksin', 'JenisVaksinController@index');

Route::get('/vaksin/create', 'JenisVaksinController@create');
Route::post('/vaksin/store', 'JenisVaksinController@store');

Route::get('/vaksin/show/{vaksin_id}', 'JenisVaksinController@edit');
Route::put('/vaksin/update/{vaksin_id}', 'JenisVaksinController@update');

Route::delete('/vaksin/delete/{vaksin_id}', 'JenisVaksinController@delete');

// Untuk Mengelola Modul Master Data Vaksin//
Route::get('/data', 'DataVaksinController@index');
Route::get('/data/store/{vaksin_id}', 'DataVaksinController@show');
Route::get('/peserta/cetak_pdf/{datavaksin_id}', 'DataVaksinController@cetak_pdf');


Route::get('/data/show/{vaksin_id}', 'DataVaksinController@show');
Route::put('/data/update/{vaksin_id}', 'JenisVaksinController@update');

Route::delete('/data/delete/{vaksin_id}', 'JenisVaksinController@delete');
});

Auth::routes();

Route::get('/', function () {
    return view('user.index');
});

Route::get('/about', function () {
    return view('user.about');
});

Route::get('/action', function () {
    return view('user.action');
});

Route::get('/news', function () {
    return view('user.news');
});

Route::get('/doctores', function () {
    return view('user.doctores');
});

Route::get('/contact', function () {
    return view('user.contact');
});



